package proj.matt.weathersummary

import cats.effect.Concurrent
import cats.implicits._
import io.circe.Encoder
import io.circe.generic.semiauto._
import org.http4s.Method._
import org.http4s._
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.implicits._
import proj.matt.weathersummary.NOAAEntities._

trait WeatherSummary[F[_]] {
  def get(latitude: Double, longitude: Double): F[WeatherSummary.Summary]
}

object WeatherSummary {
  def apply[F[_]](implicit ev: WeatherSummary[F]): WeatherSummary[F] = ev

  final case class Summary(message: String) extends AnyVal

  private object Summary {

    private def assessTemperature(noaaForecast: NOAAForecast): String = noaaForecast
      .currentTemperatureUnitOfMeasure match {
      case "C" => noaaForecast.currentTemperatureValue match {
        case v if v > 28 => "hot"
        case v if v > 20 => "moderate"
        case v if v > 10 => "cool"
        case v if v > 0 => "cold"
        case _ => "freezing"
      }
      case "F" => noaaForecast.currentTemperatureValue match {
        case v if v > 82 => "hot"
        case v if v > 68 => "moderate"
        case v if v > 50 => "cool"
        case v if v > 32 => "cold"
        case _ => "freezing"
      }
      case _ => "indeterminate"
    }

    def from(noaaForecast: NOAAForecast): Summary = {
      val message =
        s"""${noaaForecast.currentCharacteristics}
           |The current temperature is ${assessTemperature(noaaForecast)} at ${noaaForecast.currentTemperature}.
           |High of ${noaaForecast.high}. Low of ${noaaForecast.low}."""
          .stripMargin
      new Summary(message)
    }

    //noinspection ScalaUnusedSymbol (it is used)
    implicit val summaryEncoder: Encoder[Summary] = deriveEncoder[Summary]
  }

  final case class SummaryError(e: Throwable) extends RuntimeException

  def impl[F[_] : Concurrent](C: Client[F]): WeatherSummary[F] = new WeatherSummary[F] {
    val dsl: Http4sClientDsl[F] = new Http4sClientDsl[F] {}

    import dsl._

    def get(
      latitude: Double,
      longitude: Double
    ): F[Summary] = {
      C
        .expect[NOAAPoint] {
          GET(uri"https://api.weather.gov/points/" / s"$latitude,$longitude")
        }
        .map { noaaPoint =>
          Uri.fromString(noaaPoint.properties.forecastGridData) match {
            case Left(parseFailure) => throw SummaryError(parseFailure)
            case Right(forecastUrl) => forecastUrl
          }
        }
        .flatMap { noaaForecastUrl =>
          C.expect[NOAAForecast](GET(noaaForecastUrl))
            .map(Summary.from)
        }
        .adaptError { case t => NOAAError(t) }
    }
  }

}
